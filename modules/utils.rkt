#lang racket

(provide randint distance2pts)

(define (randint a b)
  (+ a (random (+ 1 (- b a)))))

(define (distance2pts x1 y1 x2 y2)
  (sqrt (+ (sqr (- x1 x2)) (sqr (- y1 y2)))))