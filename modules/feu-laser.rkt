#lang racket

;;;PROGRAMME A L'ORIGINE DE LA FLAMME EMISE PAR LE REACTEUR DU VAISSEAU

(require 2htdp/image)
(provide htable-flammes
         LASER)

(define ROUGE (make-color 255 0 0))

;;;Réel positif x Couleur x Angle(degrés) --> Image
;;;angle désigne un angle du triangle qui sera assemblé dans la fonction figure avec le résultat obtenu
;;;(crop-cercle rayon couleur angle) rend l'image d'un cercle coupé dont l'ouverture augmente avec angle
(define (crop-cercle rayon couleur angle)
  (define DY (* rayon (+ 1 (sin (* (- 90 angle) (/ pi 180))))))
  (crop 0
       0 
       (* 2 rayon) 
       DY
       (circle rayon 'solid couleur)))

;(crop-cercle 30 ROUGE 45)

;;;Réel positif x Couleur x Angle(degrés) --> Image
;;;angle désigne un angle du triangle assemblé avec le résultat de la fonction crop-cercle
;;;(figure rayon couleur angle) rend l'image d'un cercle coupé avec un triangle isocèle posé sur son ouverture
(define (figure rayon couleur angle)
  (define A (/ (- 180 angle) 2))
  (above (crop-cercle rayon couleur A)
        (triangle/aas A
                     A
                     (* rayon 2 (cos (* (- 90 A) (/ pi 180))))
                     'solid
                     couleur)))

;(figure 30 ROUGE 45)

;;;Réel positif x Couleur x Angle(degrés) --> Image
;;;angle désigne l'angle sur la pointe de la flamme
;;;(flamme rayon couleur angle) rend l'image d'une flamme obtenue par superposition grâce à la fonction figure
(define (flamme rayon couleur angle)
  (if (> (color-green couleur) 250)
     empty-image
     (underlay/align "middle"
                    "top"
                    (flamme (add1 rayon)
                           (make-color (color-red couleur)
                                      (+ 5 (color-green couleur))
                                      (color-blue couleur))
                           angle)
                    (figure rayon couleur angle))))
;(flamme 30 ROUGE 45)

;;;Réel x Réel x Couleur --> Image
;;;dx et dy représentent les dimensions d'une ellipse
;;;(laser dx dy couleur) rend l'image d'un projectile laser obtenue par superposition d'ellipses avec variation de la transparence
(define (laser dx dy couleur)
  (if (< (color-alpha couleur) 10)
     empty-image
     (underlay (laser dx (+ 2 dy) (make-color (color-red couleur)
                                              (color-green couleur)
                                              (color-blue couleur)
                                              (- (color-alpha couleur) 10)))
              (ellipse dx dy 'solid couleur))))

(define LASER (freeze (laser 5 0 (make-color 255 0 0 255))))

;;;Entier --> Hash-table
;;;n désigne la valeur maximale du compteur vitesse du vaisseau
;;;(htable-flammes n) rend une table de hash-code contenant des images de flammes
(define (htable-flammes n)
  (if (< n 0)
     (hash)
     (hash-set (htable-flammes (sub1 n))
              n
              (freeze (scale 0.2 (flamme 10 ROUGE (- 50 (* n 0.9))))))))

;(htable-flammes 41)